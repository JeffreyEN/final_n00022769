package com.xxjshenxx.final_n00022769;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.xxjshenxx.final_n00022769.Clases.Account;
import com.xxjshenxx.final_n00022769.services.AccountServices;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    ImageView imagen;
    String imgDecodableString;
    EditText imageB64;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        EditText name = findViewById(R.id.edit_name);
        EditText amount = findViewById(R.id.edit_amount);
        EditText date = findViewById(R.id.edit_date);
        EditText sucursal1 = findViewById(R.id.edit_suc1);
        EditText sucursal2 = findViewById(R.id.edit_suc2);
        EditText sucursal3 = findViewById(R.id.edit_suc3);
        imageB64 = (EditText) findViewById(R.id.edit_base64);
        Button crear = findViewById(R.id.btn_send);
        Button galeria = findViewById(R.id.btn_galeria);
        imagen = (ImageView) findViewById(R.id.imagen);

        galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarGaleria();
            }
        });

        Log.i("MIPPIN","dsd: " + imgDecodableString);

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nombre = name.getText().toString().trim();
                float monto = Float.parseFloat(amount.getText().toString().trim());
                String fecha = date.getText().toString().trim();
                String suc1 = sucursal1.getText().toString().trim();
                String suc2 = sucursal2.getText().toString().trim();
                String suc3 = sucursal3.getText().toString().trim();
                String base64 = imageB64.getText().toString().trim();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://upn.lumenes.tk")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                AccountServices service = retrofit.create(AccountServices.class);

                Account cuenta = new Account();
                cuenta.setNombre(nombre);
                cuenta.setMonto(monto);
                cuenta.setFecha_creacion(fecha);
                cuenta.setSucursal_1(suc1);
                cuenta.setSucursal_2(suc2);
                cuenta.setSucursal_3(suc3);
                cuenta.setImagen_url(base64);

                Call<Account> call = service.create(cuenta);

                call.enqueue(new Callback<Account>() {
                    @Override
                    public void onResponse(Call<Account> call, Response<Account> response) {
                        if(response.code() == 200){
                            Log.i("Web","Conexion ok");
                        }else {
                            Log.i("Web","Conexion fail");
                        }
                    }
                    @Override
                    public void onFailure(Call<Account> call, Throwable t) {
                        Log.i("Web","No pudimos conectarnos al servidor");
                    }
                });

                Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

    }
    private void cargarGaleria() { // retorna verdadero si la condicion se cumple, si tiene permisos true, sino false, se cambia en CALL_PHONE
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent.createChooser(intent,"Seleccione aplicacion"),5);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                Uri path = data.getData();
                imagen.setImageURI(path);

                final InputStream imageStream = getContentResolver().openInputStream(path);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                imgDecodableString = encodeImage(selectedImage);
                Log.i("MY_APP", "Despues de codificar: "+imgDecodableString);

            }else {
                Toast.makeText(this, "No haz elegido una imagen",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Algo salio mal", Toast.LENGTH_LONG)
                    .show();
        }
        Log.i("MY_APP", String.valueOf(resultCode));

    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        imgDecodableString = Base64.encodeToString(b, Base64.DEFAULT);
        imageB64.setText(imgDecodableString);
        return imgDecodableString;
    }
}