package com.xxjshenxx.final_n00022769.Clases;

import java.util.Date;

public class Account {
    private String nombre;
    private float monto;
    private String fecha_creacion;
    private String sucursal_1;
    private String sucursal_2;
    private String sucursal_3;
    private String imagen_url;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getSucursal_1() {
        return sucursal_1;
    }

    public void setSucursal_1(String sucursal_1) {
        this.sucursal_1 = sucursal_1;
    }

    public String getSucursal_2() {
        return sucursal_2;
    }

    public void setSucursal_2(String sucursal_2) {
        this.sucursal_2 = sucursal_2;
    }

    public String getSucursal_3() {
        return sucursal_3;
    }

    public void setSucursal_3(String sucursal_3) {
        this.sucursal_3 = sucursal_3;
    }

    public String getImagen_url() {
        return imagen_url;
    }

    public void setImagen_url(String imagen_url) {
        this.imagen_url = imagen_url;
    }
}
