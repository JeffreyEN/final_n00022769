package com.xxjshenxx.final_n00022769.services;

import com.xxjshenxx.final_n00022769.Clases.Account;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AccountServices {
    @GET("N00022769/accounts")
    Call<List<Account>> getAll();

    @POST("N00022769/accounts/")
    Call<Account> create(@Body Account cuenta);
}
