package com.xxjshenxx.final_n00022769;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.xxjshenxx.final_n00022769.Adapter.AccountAdapter;
import com.xxjshenxx.final_n00022769.Clases.Account;
import com.xxjshenxx.final_n00022769.services.AccountServices;


import java.util.List;

import javax.security.auth.callback.Callback;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Cuenta extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta);
        RecyclerView rv = findViewById(R.id.rvcuewnta);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AccountServices service = retrofit.create(AccountServices.class);
        Call<List<Account>> cuenta = service.getAll();


    }
}