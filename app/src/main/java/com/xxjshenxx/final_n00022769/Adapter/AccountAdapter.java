package com.xxjshenxx.final_n00022769.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.xxjshenxx.final_n00022769.Clases.Account;
import com.xxjshenxx.final_n00022769.R;

import java.util.List;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.AccountViewHolder>
        implements View.OnClickListener{

    List<Account> mData;
    private View.OnClickListener listener;

    public AccountAdapter(List<Account> mData) {
        this.mData = mData;
    }

    @Override
    public void onClick(View v) {

    }

    @NonNull
    @Override
    public AccountViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cuenta, parent, false);
        v.setOnClickListener(this);
        AccountAdapter.AccountViewHolder viewHolder = new AccountAdapter.AccountViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AccountViewHolder holder, int position) {
        TextView name = holder.itemView.findViewById(R.id.nombre);
        TextView monto = holder.itemView.findViewById(R.id.monto);
        TextView fecha = holder.itemView.findViewById(R.id.fecha);
        ImageView image = holder.itemView.findViewById(R.id.imageAccount);

        String nombre = String.valueOf(mData.get(position).getNombre());
        String ammount = String.valueOf(mData.get(position).getMonto());
        String date = String.valueOf(mData.get(position).getFecha_creacion());
        String imagen = String.valueOf(mData.get(position).getImagen_url());

        name.setText(nombre);
        monto.setText(ammount);
        fecha.setText(date);
        Picasso.get().load("https://upn.lumenes.tk" + imagen).into(image);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class AccountViewHolder extends RecyclerView.ViewHolder{
        public AccountViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
